import os
import sys
import random
import gi
gi.require_version('Gtk','3.0')
from gi.repository import Gtk
import time

#  $$$$$$$$\                  $$\  $$$$$$\  $$\               
#  $$  _____|                 $$ |$$  __$$\ \__|              
#  $$ |   $$\   $$\  $$$$$$\  $$ |$$ /  \__|$$\ $$$$$$\$$$$\  
#  $$$$$\ $$ |  $$ |$$  __$$\ $$ |\$$$$$$\  $$ |$$  _$$  _$$\ 
#  $$  __|$$ |  $$ |$$$$$$$$ |$$ | \____$$\ $$ |$$ / $$ / $$ |
#  $$ |   $$ |  $$ |$$   ____|$$ |$$\   $$ |$$ |$$ | $$ | $$ |
#  $$ |   \$$$$$$  |\$$$$$$$\ $$ |\$$$$$$  |$$ |$$ | $$ | $$ |
#  \__|    \______/  \_______|\__| \______/ \__|\__| \__| \__|
#

# Fuelsim 2: The Refuelling
# TODO: 
# * Fuel Station Upgrade mechanism. 
# * Time system

class Car: 
    # The car class!
    # TODO: 
    # * Fuel tank size based on vehicle size
    # * Picky Customer Behaviour (competition)
    # 
    def __init__(self):
        self.__setFuel()
        self.__setSize()
        self.__setWallet()
        self.maxPrice = 1 + (random.randint(15,30)/100)
    def __setFuel(self):
        self.fuel = random.randint(1,100)

    def __setSize(self):
        self.size = random.randint(1,4)

    def __setWallet(self):
        self.wallet = random.randint(30,500)
    
    def refuel(self, station):
        # fuel = self.fuel
        fuel = 0
        #fillVal = random.randint(65,100)
        fuelNeeded = 100-self.fuel
        if (fuelNeeded*station.price) > self.wallet and fuelNeeded < station.tank:
        # Car needs fuel, can't afford it
            fuel = int(self.wallet/station.price)
        elif fuelNeeded > station.tank and not fuel:
        # Station has not enough fuel to give, car takes what it can
            fuel = station.tank
        #elif fuelNeeded <= (100-self.fuel):
        # Car will take the fuel it needs
        #    fuel = 100-self.fuel
        else:
        # Car can't afford fuel
            fuel = (100-self.fuel)
        return (fuel)

class Station:
    # competition station
    # multiple station sytem
    # implement pumps as a subclass

    def __init__(self):
        # Game sets fuel cost
        #self.cost = []
        self.day = 0
        self.newDay()
        self.price = []
        # set max fuel tank size
        self.tankUpgrades = 0.0
        self.tankMax = 2000
        self.tank = 1000
        self.daysales = 0
        # set player money
        self.cash = 1000
    def setCost(self):
        self.cost = 1+((random.randint(15,30)/100))
    def setPrice(self,builder, station, customer):
        label = "You need to set today's fuel price. Fuel cost is $%3.2f"%self.cost
        updateMaintext(builder,label)
        box=builder.get_object('textArea')
        values=Gtk.Adjustment(1.20,1,1.30,0.01,0.1,0)
        entry = Gtk.SpinButton()
        entry.set_adjustment(values)
        #entry.set_update_policy(0)
        entry.set_numeric(1)
        entry.set_digits(2)
        #entry.set_text("Fuel Price")
        button = Gtk.Button.new_with_label("Set Price")
        button.connect("clicked", printVal, builder, entry, button, station,customer)
        box.pack_start(entry, False, True, 0)
        box.pack_start(button, False, True,0)
        builder.get_object("window1").show_all()
        #self.price = newPrice
    def tankUpgrade(self):
        self.tankMax += int(self.tankMax*0.1)
    def staff(self):
        staffCost = random.randint(25,100)
        self.cash -= staffCost
        return staffCost
    def fuelTaken(self, taken):
        self.tank -= taken
        self.cash += (self.price*taken)
    def fuelDelivery(self, wanted):
        self.cash -= (self.cost * wanted)
        self.tank += wanted
    def newDay(self):
        self.setCost()
        self.day += 1
       
def printVal(self,builder,entry,button,station,customer):
    #entry = builder.get_object('test')
    entry.update()
    val=entry.get_value()
    station.price = val
    entry.destroy()
    button.destroy()
    nextStep(builder,station,customer, 0)

def updateStatus(builder,station):
     label = "You have $%8.2f in the bank "%station.cash
     label += "and "+str(station.tank)+"L in the tank"
     builder.get_object('statusBar').set_label(label)

def updateMaintext(builder, label):
   builder.get_object('mainText').set_label(label) 

def nextStep(builder,station,customer, i):
    box = builder.get_object('textArea') 
    if builder.get_object('oldbut'):
        print("found it")
    if i==0:
        if station.day == 1:
            label = "There is a customer pulling up"
        else:
            staff = station.staff()
            label = "Yesterday's staff cost was $"+str(staff)
        updateMaintext(builder,label)
    if i==len(customer) or station.tank<=0:
        #label = "no money or fuel or customers"
        morning(builder, station, customer)
        button.destroy()
        i=0
    if station.cash<=0:
        # bankrupt!
        updateStatus(builder,station)
        label = "You are bankrupt. \n"
        label += "The bank are here to reposess your pride and joy\n"
        label += "I am sorry, I wish you better luck when you've paid off your debts"
        updateMaintext(builder, label)
    else:
        label = builder.get_object('mainText').get_label()
        label += "\nThere are "+(str(len(customer)-i)) +" more customers on the way!"
        button = Gtk.Button.new_with_label("Serve that customer!")
        button.connect("clicked", FuelCar, i, button, builder, station, customer)
        #upgbutton = Gtk.Button.new_with_label("Incease your tank capacity by 10%")
        #upgbutton.connect("clicked", station.tankUpgrade, upgbutton)
        #upgbutton.set_name("oldbut")
        box.pack_start(button, False, True, 0)
        #box.pack_start(upgbutton, False, True, 0)
        updateMaintext(builder,label)
    builder.get_object("window1").show_all()

def FuelCar(self, i, button, builder, station, customer):
    button.destroy()
    if customer[i].maxPrice>=station.price:
        refuel = customer[i].refuel(station)
        money = refuel * station.price
        station.fuelTaken(refuel)
        station.daysales += 1
        label = "That customer took "+str(refuel)+"L and spent $%5.2f"%money
    else:
        label = "Your fuel is too expensive for that customer.\n"
        label += "They will try at the competition's station over the border\n"
    
    updateMaintext(builder,label)
    updateStatus(builder,station)
    i += 1
    nextStep(builder, station, customer, i)

def newGame():
    station = Station()
    return station, genCustomer()

def genCustomer():
    customer = [1 for i in range(1, random.randint(0,100))]
    for i in range(len(customer)):
        customer[i] = Car()
    return customer

def morning(builder,station,customer):
    #check prices, costs, tank levels and so on
    station.newDay()
    label = "Good morning, it's the dawn of day "+str(station.day)+" of your fuel adventure\n"
    label += "You made "+str(station.daysales)+" sales from "+str(len(customer))+" possible customers yesterday\n"
    label += "The tanker is here to top us up, and fuel cost is $%3.2f\n"%station.cost
    label += "You have room for "+str(station.tankMax - station.tank) +"L"
    label += " and can afford "+str(int(station.cash/station.cost))+"L"
    updateMaintext(builder,label)
    builder.get_object("window1").show_all()
    buyFuel(builder,station,customer)
    #print("updated")
    #station.setPrice(builder,station,genCustomer())

def buyFuel(builder,station,customer):
        box=builder.get_object('textArea')
        values=Gtk.Adjustment(1,1,station.tankMax,1,1,0)
        entry = Gtk.SpinButton()
        entry.set_adjustment(values)
        entry.set_numeric(1)
        entry.set_digits(0)
        button = Gtk.Button.new_with_label("Buy Fuel")
        button.connect("clicked", fuelChange, builder, entry, button, station,customer)
        box.pack_start(entry, False, True, 0)
        box.pack_start(button, False, True,0)
        builder.get_object("window1").show_all()

def fuelChange(self,builder,entry,button,station,customer):
    entry.update()
    val=entry.get_value()
    if (val*station.cost) > station.cash:
        label = "You don't have enough cash for that"
        updateMaintext(builder,label)
        builder.get_object("window1").show_all()
        fuelChange(builder,entry,button,station,customer)
    elif val > (station.tankMax-station.tank):
        label = "you don't have room for that!"
        updateMaintext(builder,label)
        builder.get_object("window1").show_all()
        fuelChange(builder,entry,button,station,customer)
    else:
        button.destroy()
        entry.destroy()
        station.tank += val
        station.cash -= (val*station.cost)
    updateStatus(builder,station)
    station.setPrice(builder,station,genCustomer())
